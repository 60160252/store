/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author Golf
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO RECEIPT (CUS_ID,USER_ID,REC_TOTAL)VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getId());
            stmt.setInt(2, object.getSeller().getId());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO RECEIPT_DETAIL (REC_ID,PD_ID,DETAIL_REC_PRICE,DETAIL_REC_AMOUNT)\n"
                        + "                           VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getReceipt().getId());
                stmtDetail.setInt(2, r.getProduct().getId());
                stmtDetail.setDouble(3, r.getPrice());
                stmtDetail.setInt(4, r.getAmount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmtDetail.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt");
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.REC_ID as REC_ID,\n"
                    + "       REC_CREATE,\n"
                    + "       c.CUS_ID as CUS_ID,\n"
                    + "       c.CUS_NAME as CUS_NAME,\n"
                    + "       c.CUS_TEL as CUS_TEl,\n"
                    + "       u.USER_ID as USER_ID,\n"
                    + "       u.USER_NAME as USER_NAME,\n"
                    + "       u.USER_TEL as USER_TEL,  \n"
                    + "       REC_TOTAL\n"
                    + "  FROM RECEIPT r, CUSTOMER c, USER u\n"
                    + "  WHERE r.CUS_ID = c.CUS_ID AND r.USER_ID = u.USER_ID"
                    + "  ORDER BY REC_CREATE DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("REC_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("REC_CREATE"));
                int customerId = result.getInt("CUS_ID");
                String customerName = result.getString("CUS_NAME");
                String customerTel = result.getString("CUS_TEL");
                int userId = result.getInt("USER_ID");
                String userName = result.getString("USER_NAME");
                String userTel = result.getString("USER_TEL");
                double total = result.getDouble("REC_TOTAL");
                Receipt receipt = new Receipt(id, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select all receipt" + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date paring all receipt" + ex.getMessage());
        }

        db.close();
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT r.REC_ID as REC_ID,\n"
                    + "       REC_CREATE,\n"
                    + "       c.CUS_ID as CUS_ID,     \n"
                    + "       c.CUS_NAME as CUS_NAME,\n"
                    + "       c.CUS_TEL as CUS_TEl,\n"
                    + "       u.USER_ID as USER_ID,\n"
                    + "       u.USER_NAME as USER_NAME,\n"
                    + "       u.USER_TEL as USER_TEL,  \n"
                    + "       REC_TOTAL\n"
                    + "  FROM RECEIPT r, CUSTOMER c, USER u\n"
                    + "  WHERE r.REC_ID = ? AND r.CUS_ID = c.CUS_ID AND r.USER_ID = u.USER_ID;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int rid = result.getInt("REC_ID");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("REC_CREATE"));
                int customerId = result.getInt("CUS_ID");
                String customerName = result.getString("CUS_NAME");
                String customerTel = result.getString("CUS_TEL");
                int userId = result.getInt("USER_ID");
                String userName = result.getString("USER_NAME");
                String userTel = result.getString("USER_TEL");
                double total = result.getDouble("REC_TOTAL");
                Receipt receipt = new Receipt(rid, created,
                        new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));

                getReceiptDetail(conn, id, receipt);
                conn.commit();

                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id" + id + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date paring all receipt" + ex.getMessage());
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT rd.DETAIL_REC_ID as DETAIL_REC_ID,\n"
                + "       REC_ID,\n"
                + "       rd.PD_ID as PD_ID,\n"
                + "       p.PD_NAME as PD_NAME,\n"
                + "       p.PD_PRICE as PD_PRICE,\n"
                + "       rd.DETAIL_REC_PRICE as DETAIL_REC_PRICE,\n"
                + "       DETAIL_REC_AMOUNT\n"
                + "  FROM RECEIPT_DETAIL rd, PRODUCT p\n"
                + "  WHERE REC_ID = ? AND rd.PD_ID = p.PD_ID;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while(resultDetail.next()){
            int receiveId = resultDetail.getInt("PD_ID");
            int productId = resultDetail.getInt("PD_ID");
            String productName = resultDetail.getString("PD_NAME");
            double productPrice = resultDetail.getDouble("PD_PRICE");
            double price = resultDetail.getDouble("DETAIL_REC_PRICE");
            int amount = resultDetail.getInt("DETAIL_REC_AMOUNT");
            Product product = new Product(productId, productName, productPrice);
            receipt.addReceiptDetail(receiveId,product, amount,price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM RECEIPT WHERE REC_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id" + id);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE PRODUCT SET PD_NAME = ?,PD_PRICE = ? WHERE PD_ID = ?";
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getName());
//            stmt.setDouble(2, object.getPrice());
//            stmt.setInt(3, object.getId());
//            row = stmt.executeUpdate();
//        } catch (SQLException ex) {
//            System.out.println("Error: SQL error");
//        }
//
//        db.close();
        return 0;
    }

    public static void main(String args[]) {
        Product p1 = new Product(1, "Americano", 50);
        Product p2 = new Product(2, "Espresso", 50);
        User seller = new User(1, "kimtae", "0881000000");
        Customer customer = new Customer(1, "pepper", "0881003131");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);

        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id= " + dao.add(receipt));
        System.out.println("Receipt after add " + receipt);
        System.out.println("Get all " + dao.getAll());

        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New receipt: " + newReceipt);
        
        dao.delete(receipt.getId());
    }

}
