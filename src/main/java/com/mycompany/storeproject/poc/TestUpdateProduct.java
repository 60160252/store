/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Golf
 */
public class TestUpdateProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "UPDATE PRODUCT SET PD_NAME = ?,PD_PRICE = ? WHERE PD_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, "ThaiTea");
            stmt.setDouble(2, 40);
            stmt.setInt(3, 9);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            System.out.println("Error: SQL error");
        }

        db.close();

    }
}
